from django.db import models
from django.contrib.auth.models import AbstractUser, BaseUserManager
from django.db.models import signals
from django.db.models.signals import post_save, pre_save, pre_delete
from django.dispatch import receiver
from datetime import datetime
# https://www.fomfus.com/articles/how-to-use-email-as-username-for-django-authentication-removing-the-username
from django.utils.translation import ugettext_lazy as _
from django_neomodel import DjangoNode #DjangoRel class is created in this file, not imported
from django.db.models.options import Options #DjangoRel class needs this
from neomodel import *
from django.conf import settings #required for adding DjangoRel relationships
from s3upload.fields import S3UploadField
import pdb




#====================================== POSTGRESQL ======================================

class UserManager(BaseUserManager):
  # define a model manager for User model with no username field.
  use_in_migrations = True

  def _create_user(self, email, password, **extra_fields):
    # create and save a User with the given email and password.
    if not email:
      raise ValueError('The given email must be set')
    email = self.normalize_email(email)
    user = self.model(email=email, **extra_fields)
    user.set_password(password)
    user.save(using=self._db)
    return user

  def create_user(self, email, password=None, **extra_fields):
    # create and save a regular User with the given email and password.
    extra_fields.setdefault('is_staff', False)
    extra_fields.setdefault('is_superuser', False)
    return self._create_user(email, password, **extra_fields)

  def create_superuser(self, email, password, **extra_fields):
    # create and save a SuperUser with the given email and password.
    extra_fields.setdefault('is_staff', True)
    extra_fields.setdefault('is_superuser', True)

    if extra_fields.get('is_staff') is not True:
      raise ValueError('Superuser must have is_staff=True.')
    if extra_fields.get('is_superuser') is not True:
      raise ValueError('Superuser must have is_superuser=True.')

    return self._create_user(email, password, **extra_fields)




class User(AbstractUser):
  username = None
  email = models.EmailField(_('email address'), unique=True)

  USERNAME_FIELD = 'email'
  REQUIRED_FIELDS = []

  objects = UserManager()




# 2018-April https://www.youtube.com/watch?v=lxSZevvkcc4
# extends the user model to include extra fields in a one to one with Profile
class Profile (models.Model):
  user = models.OneToOneField(User, on_delete=models.CASCADE)
  email_confirmed = models.BooleanField(default=False)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  #linkedin_profile = models.CharField(max_length=100, default='') # if in profile then can define once and use at multiple orgs?

  def __str__(self):
    return self.user.email


@receiver(post_save, sender=User)
#def create_user_profile(sender, instance, created, **kwargs):
def update_user_profile(sender, instance, created, **kwargs):
  if created:
    Profile.objects.create(user=instance)
  instance.profile.save()

@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
  instance.profile.save()




class Organization(models.Model):
  name = models.CharField(max_length=60)
  domain = models.CharField(max_length=250)
  graph_path = models.CharField(max_length=2083, default='')
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)

  profiles = models.ManyToManyField(
    Profile, 
    related_name='organizations',
    through='Membership',
    through_fields=('organization', 'profile')
  )
  # when you specify related_name, django lets you do `user.profile.organizations.all()`
  # without a related name you can still do `user.profile.organization_set.all()`
  def __str__(self):
    return self.name




# 2018-06-27 https://stackoverflow.com/questions/51071081/django-manytomanyfield-how-to-add-created-at-and-updated-at/51071153#51071153
# this class is established through param on ManyToManyField
# it lets you add additional fields to your many to many table
class Membership(models.Model):
  organization = models.ForeignKey(Organization, on_delete=models.CASCADE)
  profile = models.ForeignKey(Profile, on_delete=models.CASCADE)
  created_at = models.DateTimeField(auto_now_add=True)
  updated_at = models.DateTimeField(auto_now=True)




#====================================== NEO4J ======================================

#-------------------------- SCHEMA ---------------------------

# 2018-04-24 extend django_neomodel's StructuredRel because django_neomodel only has DjangoNode not DjangoRel
# https://drive.google.com/file/d/1HKkLqkG6_5-HM8FFHA5KabQs4WeRqT9l/view?usp=sharing
class DjangoRel(StructuredRel):
  def __init__(self, *args, **kwargs):
    super(DjangoRel, self).__init__(*args, **kwargs)

  __all_properties__ = ()

  @classproperty
  def _meta(self):
    if hasattr(self.Meta, 'unique_together'):
      raise NotImplementedError('unique_together property not supported by neomodel')

    opts = Options(self.Meta, app_label=self.Meta.app_label)
    opts.contribute_to_class(self.__class__, self.__class__.__name__)

    for key, prop in self.__all_properties__:
      opts.add_field(DjangoField(prop, key), getattr(prop, 'private', False))

    return opts

  def pre_save(self):
    if getattr(settings, 'NEOMODEL_SIGNALS', True):
      self._creating_node = getattr(self, 'id', None) is None
      signals.pre_save.send(sender=self.__class__, instance=self)

  def post_save(self):
    if getattr(settings, 'NEOMODEL_SIGNALS', True):
      created = None
      #delattr(self, '_creating_node')
      signals.post_save.send(sender=self.__class__, instance=self, created=created)

  def pre_delete(self):
    if getattr(settings, 'NEOMODEL_SIGNALS', True):
      signals.pre_delete.send(sender=self.__class__, instance=self)

  def post_delete(self):
    if getattr(settings, 'NEOMODEL_SIGNALS', True):
      signals.post_delete.send(sender=self.__class__, instance=self)




"""
-------------------------- RELATIONSHIP ---------------------------
  - defined high up in the file in case the model resources below need them defined unlazily
  - also there are more edges in a graph than there are nodes. interesting concept... more relationships than there are resources. 
  - it's mathematically for keeping track of resources
"""

class BossRel(DjangoRel):
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)
  #weight = IntegerProperty(default=None, required=False,)

  class Meta:
    app_label = 'django_rel'

  # overriding save
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    super(BossRel, self).save(*args, **kwargs)




class DepartmentMemberRel(DjangoRel):
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)
  #weight = IntegerProperty(default=None, required=False,)

  class Meta:
    app_label = 'django_rel'

  # overriding save
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    super(DepartmentMemberRel, self).save(*args, **kwargs)




class DepartmentManagerRel(DjangoRel):
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)

  class Meta:
    app_label = 'django_rel'

  # overriding save
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    super(DepartmentManagerRel, self).save(*args, **kwargs)




class BranchRel(DjangoRel):
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)
  #weight = IntegerProperty(default=None, required=False,)

  class Meta:
    app_label = 'django_rel'

  # overriding save
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    super(BranchRel, self).save(*args, **kwargs)




class OfficeEmployeeRel(DjangoRel):
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)
  #weight = IntegerProperty(default=None, required=False,)
  #main_workplace = BooleanProperty(default=True)
  #do they have a workspace
  #workspace number
  #percent of their time spent there

  class Meta:
    app_label = 'django_rel'

  # overriding save
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    super(OfficeEmployeeRel, self).save(*args, **kwargs)




class OfficeManagerRel(DjangoRel):
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)

  class Meta:
    app_label = 'django_rel'

  # overriding save
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    super(OfficeManagerRel, self).save(*args, **kwargs)




class DepartmentOfficeRel(DjangoRel):
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)

  class Meta:
    app_label = 'django_rel'

  # overriding save
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    super(DepartmentOfficeRel, self).save(*args, **kwargs)




#-------------------------- EMPLOYEE ---------------------------

class Employee(DjangoNode):
  
  # ------------ METADATA ATTRIBUTES ------------
  uid = UniqueIdProperty()
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)
  date_hired = DateProperty(required=False, default=None)
  # employee_number = 

  # ------------ CONTACT ATTRIBUTES ------------
  """
    $`email_work` is used as identifier when adding bosses/ depts 
    because employees know it, whereas they would not know the uid.
    $ It is also need for mass onboarding via manifest file 
    because the uid will not exist at that point and you need an identifier.
  """
  email_work = EmailProperty(required=True, unique_index=True, max_length=70,)
  # phone_work_cell = StringProperty(max_length=250, required=False, default=None,)
  # phone_work_desk = StringProperty(max_length=250, required=False, default=None,)
  username_slack = StringProperty(max_length=25)

  # ------------ NAME ATTRIBUTES ------------
  name_prefix = StringProperty(max_length=25) # (Dr, Rev, Maj, Senator)
  name_first = StringProperty(required=True, max_length=25)
  name_prefer = StringProperty(max_length=25)  # nickname, or whatever they actually want to be called
  name_middle = StringProperty(max_length=25)
  name_last = StringProperty(required=True, max_length=50)

  """
    $ Something like `def full_name(self):` is not sufficient because js libs
    call it by field and we need some pre_save checks for dynamic full_names
    $ These boolean checks get run in pre_save to update name_full
  """
  name_full = StringProperty(max_length=102)
  name_is_prefer_used = BooleanProperty(default=False)
  name_is_middle_used = BooleanProperty(default=False)
  name_is_prefix_used = BooleanProperty(default=False)

  profile_picture = StringProperty(max_length=500)

  # ------------ JOB ATTRIBUTES ------------
  job_title = StringProperty(max_length=100)
  job_description = StringProperty(max_length=500)
  # job_ask_me_about = StringProperty(max_length=1000, required=False, default=None,)
  # job_responsible_for = StringProperty(max_length=1000, required=False, default=None,)


  # ------------ Relationships ------------
  bosses = RelationshipTo('Employee', 'REPORTS_TO', model=BossRel)
  direct_reports = RelationshipFrom('Employee', 'REPORTS_TO', model=BossRel)
  departments = RelationshipTo('Department', 'MEMBER_OF', model=DepartmentMemberRel)
  departments_managed = RelationshipFrom('Department', 'OVERSEEN_BY', model=DepartmentManagerRel)
  offices = RelationshipTo('Office', 'WORKS_FROM', model=OfficeEmployeeRel)
  offices_managed = RelationshipFrom('Office', 'ADMINISTERED_BY', model=OfficeManagerRel)

  # ------------ Model Settings ------------
  # DjangoNode class must have a Meta class and documentation specifies 'django_node'
  class Meta:
    app_label = 'django_node'


  # overriding save so that updted_at can be added
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    self.email_work = self.email_work.lower() # if you look at your gmail, all of the addresses are lowercases
    super(Employee, self).save(*args, **kwargs)


# before employee is saved, apply any changes to full name based on their preferences
@receiver(pre_save, sender=Employee)
def change_name_full(sender, instance, **kwargs):
  # autofill the preferred name with the first name if they do not provide it
  # this means that you should not check for `name_is_prefer_used` before running pre_save
  if not instance.name_prefer:
    instance.name_prefer = instance.name_first

  # do they want to use their nickname instead of their real first name? either way, they will always be called something in the first name slot
  if instance.name_is_prefer_used == True:
    call_me = instance.name_prefer
  else:
    call_me = instance.name_first

  # case where they want neither prefix nor middle name used
  if (instance.name_is_prefix_used == False) and (instance.name_is_middle_used == False):
    instance.name_full = f"{call_me} {instance.name_last}"

  # case where they want both their prefix and middle name used
  elif (instance.name_is_prefix_used == True) and (instance.name_is_middle_used == True):
    instance.name_full = f"{instance.name_prefix} {call_me} {instance.name_middle} {instance.name_last}"

  # case where they want their prefix but not middle name used
  elif (instance.name_is_prefix_used == True) and (instance.name_is_middle_used == False):
    instance.name_full = f"{instance.name_prefix} {call_me} {instance.name_last}"

  # case where they want their middle name used, but not their prefix
  elif (instance.name_is_prefix_used == False) and (instance.name_is_middle_used == True):
    instance.name_full = f"{call_me} {instance.name_middle} {instance.name_last}"

@receiver(pre_save, sender=Employee)
def check_for_profile_pic(sender, instance, **kwargs):
  if instance.profile_picture == "":
    instance.profile_picture = "https://s3.amazonaws.com/orgdb-media/person-square.jpg"


  """  
  skill_1 =  StringProperty(max_length=20)
  skill_1_rating =  IntegerProperty(default=5)
  skill_2 =  StringProperty(max_length=20)
  skill_2_rating =  IntegerProperty(default=5)
  skill_3 =  StringProperty(max_length=20)
  skill_3_rating =  IntegerProperty(default=5)
  skill_4 =  StringProperty(max_length=20)
  skill_4_rating =  IntegerProperty(default=5)
  skill_5 =  StringProperty(max_length=20)
  skill_5_rating =  IntegerProperty(default=5)
  """




"""
  django-s3-upload package -- https://github.com/yunojuno/django-s3-upload

  $ By default, this creates a file table, but does not write to it
  so i added `class Meta: create_db_table = False` to forms.py `S3Upload`
  and now it does not create an empty table on migrate
"""
class File(models.Model):
  profile_pic = S3UploadField(dest='media_folder')




#-------------------------- DEPARTMENT ---------------------------

class Department(DjangoNode):
  
  # ------------ METADATA ------------
  uid = UniqueIdProperty()
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)
  # date_founded = DateTimeProperty(default=datetime.now)

  # ------------ ATTRIBUTES ------------
  name_long = StringProperty(required=True, max_length=65)
  name_short = StringProperty(required=True, unique_index=True, max_length=51) # can filter to chop long names in view
  mission_statement = StringProperty(max_length=500)
  # turning unique_index to False because it is getting required by form
  email_distribution = StringProperty(required=False, unique_index=False, max_length=254)
  channel_slack = StringProperty(max_length=25)

  # ------------ RELATIONSHIPS ------------
  members = RelationshipFrom('Employee', 'MEMBER_OF', model=DepartmentMemberRel)
  overseers = RelationshipTo('Employee', 'OVERSEEN_BY', model=DepartmentManagerRel)
  branches = RelationshipFrom('Department', 'BRANCH_OF', model=BranchRel)
  parent_departments = RelationshipTo('Department', 'BRANCH_OF', model=BranchRel) # parents seems like a reserved word
  offices = RelationshipTo('Office', 'BASED_IN', model=DepartmentOfficeRel)

  # ------------ Model Settings ------------
  # DjangoNode class must have a Meta class and documentation specifies 'django_node'
  class Meta:
    app_label = 'django_node'

  # overriding save so that updted_at can be added
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    self.name_short = self.name_short.lower()
    super(Department, self).save(*args, **kwargs)




#-------------------------- OFFICE ---------------------------

class Office(DjangoNode):
  # ------------ METADATA ------------
  uid = UniqueIdProperty()
  created_at = DateTimeProperty(default_now=True)
  updated_at = DateTimeProperty(default_now=True)
  date_opened = DateProperty(default_now=True)

  # ------------ ATTRIBUTES ------------
  name_long = StringProperty(required=True, max_length=65)
  name_short = StringProperty(required=True, unique_index=True, max_length=51) # can filter to chop long names in view
  
  address_name = StringProperty(max_length=50)
  address_street = StringProperty(max_length=50)
  address_street_ext = StringProperty(max_length=50)
  address_city = StringProperty(max_length=50)
  address_state = StringProperty(max_length=50)
  address_zip = StringProperty(max_length=25)
  address_country = StringProperty(max_length=50)

  phone_main_desk = StringProperty(max_length=25)
  email_distribution = StringProperty(required=False, unique_index=False, max_length=254)
  channel_slack = StringProperty(max_length=25)

  workspaces_maximum = IntegerProperty()

  # ------------ RELATIONSHIPS ------------
  employees = RelationshipFrom('Employee', 'WORKS_FROM', model=OfficeEmployeeRel)
  site_managers = RelationshipTo('Employee', 'ADMINISTERED_BY', model=OfficeManagerRel)
  departments = RelationshipFrom('Department', 'BASED_IN', model=DepartmentOfficeRel)

  # ------------ Model Settings ------------
  # DjangoNode class must have a Meta class and documentation specifies 'django_node'
  class Meta:
    app_label = 'django_node'

    # overriding save so that updted_at can be added
  def save(self, *args, **kwargs):
    self.updated_at = datetime.now()
    self.name_short = self.name_short.lower()
    super(Office, self).save(*args, **kwargs)
