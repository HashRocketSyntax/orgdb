// This js support profile-picture.html which is in the other root templates dir

// node to observe
const picLink = $('.s3upload')[0];

// https://developer.mozilla.org/en-US/docs/Web/API/MutationRecord
const observer = new MutationObserver(
    function(mutations) {
        mutations.forEach(
            function (mutation) {
                // look for mutations on the attributes specified in observer config
                if (mutation.type == 'attributes') {
                    
                    // when the form is active
                    if ( $('.s3upload').hasClass( "form-active" ) ){
                        $('#saver').css('display', 'none'); // hide save button
                        $('.imaj').css('display', 'none'); // hide pic
                    }

                    // when the picture link is active 
                    if ( $('.s3upload').hasClass( "link-active" ) ){
                        // grab the value of the s3 link
                        var imajLink = $('.s3upload__file-link').attr('href');
                        // show the saver form
                        $('#saver').css('display', 'block');
                        // add hidden input with value of s3 link
                        $('#saver').prepend($('<input>',{
                            type: 'hidden',
                            id: 's3_link',
                            name: 's3_link',
                            value: imajLink
                        }))
                        // if the new pic already exists don't add it to page
                        if ( $('.imaj').length < 1 ) {
                            // add img to page using the s3 link
                            $('#imaj-target').prepend($('<img>',{
                                class: 'imaj',
                                src: imajLink,
                                width: 50
                            }))
                        }
                        // zen hide ze link!
                        $('.s3upload__file-link').css('display', 'none');
                    }
                }
            }
        )
    }
)

// observer config
observer.observe(
    picLink, 
    {
        // things related to picLink to watch for changes
        attributes: true, 
        attributeFilter: ['class'],
        childList: false, 
        characterData: true,
        attributeOldValue: true
    }
)
