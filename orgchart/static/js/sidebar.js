// this one is for the main menu sidebar on the left
$(document).ready(function() {
  $('.nav-trigger').click(function() {
    $('.side-nav').toggleClass('visible');
    $('.side-nav .logo').toggle();
    /*
        $('.side-nav').style.zIndex = "3";
        
        this works, but was causing an error
        i'm commenting it out 6 months later and it seems to work without it
        but let's not delete it because z-index can be a pain to figure out
        and i would never think to add this back in
    */
  });
});
