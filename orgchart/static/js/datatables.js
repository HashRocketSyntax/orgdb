$(document).ready(function () {
    $('#department-list-tbl').DataTable({
        "paging": false,
        "info": false,
    });
});
/* 
    using this for striped table rows, search, and column sort in list pages
    https://mdbootstrap.com/content/tables/ 
*/
