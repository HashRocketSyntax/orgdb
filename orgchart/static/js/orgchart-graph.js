$(document).ready(function() { 
    /* 
        physics to stabilize graph -- stackoverflow.com/a/32522961/5739514
    */
    var container = document.getElementById('graph');

    // https://gist.githubusercontent.com/ikwattro/8a8b0a6cfe4910db20b7/raw/de4fbbeecae349338aea065bd96a25913ed78301/graph.json
    var json = $.getJSON("need-orgchart-data")
        .done(function(data){
            console.log(data.orgchart_data.nodes)
            var graph_data = {
                nodes: data.orgchart_data.nodes,
                edges: data.orgchart_data.edges
            };

            var options = {
                groups: {
                    Employee: {
                        size: 40,
                        color: {
                            border: 'DARKSLATEGRAY',
                            hover: {
                                border: '#20b2aa'
                            }
                        },
                        font: {
                            size: 17,
                            face: 'avenir',
                            color: 'silver',
                        },
                        shape: 'circularImage',
                        brokenimage: 'https://s3.amazonaws.com/orgdb-assets/media/profile-icon-square.png',
                    },
                    Department: {
                        size: 55,
                        image: 'static/images/department-icon.png',
                        shape: 'image',
                        font: {
                            size: 22,
                            face: 'avenir',
                            color: 'white'
                        },
                        color: {
                            background: 'orange',
                            border: 'DARKGRAY',
                            highlight: {
                                background:'orange',
                                border: '#d66a00'
                            },
                            hover: {
                                background:'orange',
                                border: '#d66a00'
                            }
                        }
                    },
                    Office: {
                        size: 75,
                        image: 'static/images/office-icon.png',
                        shape: 'image',
                        font: {
                            size: 24,
                            face: 'avenir',
                            color: 'white'
                        },
                        color: {
                            background: 'orange',
                            border: 'DARKGRAY',
                            highlight: {
                                background:'orange',
                                border: '#d66a00'
                            },
                            hover: {
                                background:'orange',
                                border: '#d66a00'
                            }
                        }
                    }
                },
                nodes: {
                    shape: 'circle',
                    borderWidth: 3,
                    borderWidthSelected: 4,
                    mass: 2
                },
                edges: {
                    arrows: {
                        to: {
                            enabled: true,
                            scaleFactor: 2
                        },
                    }
                },
                layout: {
                    randomSeed: 4// same layout every time
                },
                physics: {
                    solver: 'barnesHut',
                    barnesHut: {
                      gravitationalConstant: -48000,
                      centralGravity: 6.5,
                      springLength: 75,
                      springConstant: 0.035,
                      avoidOverlap: 1,
                      damping: 1 //max
                    },
                    /*forceAtlas2Based: {
                        gravitationalConstant: -26,
                        centralGravity: 0.005,
                        springLength: 230,
                        springConstant: 0.18,
                        avoidOverlap: 1.5
                    },*/
                    /*repulsion: {
                      centralGravity: 0.005,
                      springLength: 280,
                      springConstant: 0.06,
                      nodeDistance: 200,
                      damping: 0.09
                    },*/
                    maxVelocity: 115,
                    timestep: 0.35,
                    stabilization: {    
                        enabled: true,
                        iterations: 1000,
                        updateInterval: 25,
                        fit: true
                    }
                },
                interaction: {
                    hover: true,
                    keyboard: true
                }
            };

            var network = new vis.Network(container, graph_data, options);
            
            network.on("stabilizationIterationsDone", function () {
                network.setOptions();
            });



            /* ========= GRAPH BUTTONS ========= */
            // this gives each button a hover that is independent of the rest of the class
            $('.graph-button').each(function(){
                $(this).hover( 
                    function(){
                        $(this).css('margin-left', '19px');
                        $(this).css('font-size', '13px');
                        $(this).find("i").css('padding', '6px');
                        $(this).find("i").css('opacity', '.6');
                    },
                    function(){
                        $(this).css('margin-left', '15px');
                        $(this).css('font-size', '12px');
                        $(this).find("i").css('padding', '5px');
                        $(this).find("i").css('opacity', '.55');
                    }
                );
            });

            // move tooltip with mouse -- https://jqueryui.com/tooltip/#tracking
            $(document).ready(function() {
                $('.graph-button').tooltip({ 
                    track: true,
                    show: {delay: 500}
                });
            });

            $('#reframe-button').click(function(){
                network.fit();
            });

            /*
            $('#zoom-in-button').click(function() {
                var canvas = $('.vis-network canvas');
                canvas.scrollLeft = 0;
                canvas.scrollTop = 100;
                console.log("zoom it");
            });
            */
            /*
            $('#zoom-in-button').click(function clusterOutliers() {
                // network.setData(data);
                network.clusterOutliers();
            });
            // ^ this works but we don't want it to cluster departments... 
            // we just want employees clustered into departments
            */



            // ============ CLICK ON A NODE ============
            // visjs events api -- http://visjs.org/docs/network/#Events
            network.on("selectNode", function (params) {
                // open a node if it is a cluster
                if (params.nodes.length == 1) {
                    if (network.isCluster(params.nodes[0]) == true) {
                        network.openCluster(params.nodes[0])
                    }
                }

                // params contains array of clicked nodes
                var canvas_nodeId = params.nodes[0];
                var node_data = graph_data.nodes.filter(function(n) {
                    return n.id == canvas_nodeId;
                });
                var node_uid = node_data[0].uid
                var node_group = node_data[0].group


                /*
                    $ Rather than making a different ajax request for each 
                    node_group we're going to make one call to views.py and 
                    return different data according to the node_group.
                */
                function NeedNodeData(node_uid, node_group) {
                    $.ajax({
                        type: "GET",
                        url: "need-node-data",
                        dataType: "json",
                        async: true,
                        data: {
                            'node_uid': node_uid,
                            'node_group': node_group,
                            // noticing this key doesnt have quotes
                            csrfmiddlewaretoken: '{{ csrf_token }}'
                        },
                        success: function (response) {
                            //console.log(response)
                            RightSidebar(response)
                        }
                    });
                }

                function RightSidebar(response) {
                    $("#sidebar-main").show("slide", { direction: "right" }, 150);

                    $(".close-right-panel").click(function(){
                        $("#sidebar-main").hide("slide", { direction: "right" }, 150);
                    });

                    // if you click outside of the div, it will close it
                    $(document).on('click', function(e) {
                        if(!$(e.target).closest('#sidebar-main').length) {
                            if ($("#sidebar-main").is(':visible')) {
                                $("#sidebar-main").hide("slide", { direction: "right" }, 150);
                            }
                       }
                    });

                    SidebarContent(response);
                }

                function SidebarContent(response) {
                    /*
                        $ This make the json response object in views.py 
                        optionally available to the DOM. If you use the attribute
                        `data-resp=json-key-name` then you will get the 
                        json-key-value in the .text() of the element.
                        $ Without this function you would have to hardcode each 
                        line: `('#employee-email').html(response['email_work']);` 
                    */
                    
                    $('[data-resp]').each(function() {
                        $(this).html( response[$(this).attr('data-resp')] )
                    });

                    /*
                        $ This .map builds on that by automatically creating 
                        rows for anything in the details:{} part of the response
                        $ Note the data-resp in the span
                        $ Without the .empty the .append will inherit rows with
                        each additional call
                    */
                    $("#details-tbl").empty();
                    $.map(response.details, function(val, key) {
                        $("#details-tbl").append([
                            $("<tr>"),
                            $("<td>").addClass('detail-key').text(key),
                            $("<td>").addClass('detail-value-holder').append(
                                $("<span>").addClass('detail-value').attr('data-resp', key).text(val)
                            )
                        ]);
                    });

                    /* 
                        $ here is the desired html of that crazy .map()
                        <tr>
                            <td class="detail-key">
                                // [KEY] e.g. "Email"
                            </td>
                            <td class="detail-value-holder">
                                <span class="detail-value" data-resp="[KEY]">
                                    // [VALUE] e.g. "lsadler@wuxinextcode.com"
                                </span>
                            </td>
                        </tr>
                    */

                    /*
                        $ But at this point we still need different content 
                        based on the node_group. For example the skills chart 
                        for employees does not apply to departments
                    */
                    $(".chart-skills").empty();
                    if (response['node_group'] == "Employee") {
                        EmployeeContent(response);
                    } else if (response['node_group'] == "Department") {
                        DepartmentContent(response);
                    } else if (response['node_group'] == "Office") {
                        OfficeContent(response);
                    }

                    function EmployeeContent(response) {
                        $("#pic").attr("src", response.pic);
                        $("#name").attr("href", "employee/detail/" + response.uid);
                        $("#section-head-main").text("Job Description");
                        
                        $(".chart-skills").append(
                            $("<canvas>").attr('id', 'chartRadar')
                        );
                        skillChart();
                    }
                    function DepartmentContent(response) {
                        $("#pic").attr("src", response.pic);
                        $("#name").attr("href", "department/detail/" + response.uid);
                        $("#section-head-main").text("Mission Statement");
                    }
                    function OfficeContent(response) {
                        $("#pic").attr("src", response.pic);
                        $("#name").attr("href", "office/detail/" + response.uid);
                        //$("#section-head-main").text("Mission Statement");
                    }
                }

                NeedNodeData(node_uid, node_group);
            });
        });
});
/* 
    $ This is where jquery starts to break down
    I need to be able to access the raw response in the template {{}}
    to render different content based on node_group
    but according to js experts... that is not possible
    as Django has already rendered the view data {{}}
    so the only option is to pass in updated js.
    
    $ So my options are (a) rebuild the app in react/vue/angular
    (b) write the html in jquery syntax (c) write the html
    in some js templating lang like handlebarsjs. I'm going
    to do option b since c would require new syntax anyways
    and additional dependencies.
*/
