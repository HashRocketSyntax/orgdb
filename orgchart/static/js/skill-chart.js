function skillChart() { 
  Chart.defaults.global.legend.display = false;
  Chart.defaults.global.defaultFontColor = 'rgba(156,63,74, 1)';

  var chartRadarDOM = $('#chartRadar');
  var chartRadarData = {
    labels: ["Agile Product Management", "Product Marketing", "Sales & Demos", "Network Architecture", "Software Development"],
    datasets: [{
      label: "Skill Level",
      backgroundColor: "rgba(156,63,74,.5)",
      borderColor: "rgba(156,63,74,.8)",
      pointBackgroundColor: "rgba(156,63,74,1)",
      pointBorderColor: "#fff",
      pointHoverBackgroundColor: "#fff",
      pointHoverBorderColor: "rgba(156,63,74,1)",
      pointBorderWidth: 2,
      data: [5, 4, 3, 2, 3]
    }]
  };
  var chartRadarOptions = {
    responsive: true,
    scale: {
      ticks: {
        beginAtZero: true,
        maxTicksLimit: 5
      },
      pointLabels: {
        fontSize: 12
      },
      gridLines: {
        color: 'rgba(156,63,74,.1)'
      }
    }

  };
  var chartRadar = new Chart(chartRadarDOM, {
    type: 'radar',
    data: chartRadarData,
    options: chartRadarOptions
  });
};
