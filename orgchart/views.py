from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse
from django.urls import reverse
from django.contrib.sites.shortcuts import get_current_site
from django.utils.encoding import force_bytes
from django.utils.encoding import force_text
from django.utils.http import urlsafe_base64_encode, urlsafe_base64_decode
from django.template.loader import render_to_string
from orgchart.tokens import account_activation_token
from django.dispatch import receiver
from django.contrib.auth import login, authenticate
from django.contrib.auth.signals import user_logged_out
from django.contrib import messages
from django.views.generic import RedirectView
from datetime import datetime
import json
import unidecode
from unidecode import unidecode
from .models import *
from .forms import *
from neomodel import *
import pprint
import numpy
import pdb

from django.views.generic import FormView

"""
------ Notes about views.py ------ 
Yes, I am well aware of Django's class-based views. I choose not to use them 
1. My view names are explicit. It helps me focus on the resources and what they do, rather than DRY code
2. I like reading view functions top to bottom
3. I hate messing with self referencing init mumbo jumbo
4. I like context to just be `context` if I need it
5. You still have to specify POST, GET, DELETE on custom views and in update views
6. Even with this autoform stuff, you still have to have error messages/ cleaning

I also like having all views in 1 file:
1. Easily copy/ paste code, and see how you did stuff elsewhere
2. Less aimless scrolling of file tree
3. Easier to keep track of dependencies imported

`return redirect` hits the view, whereas `return render` goes straight to the template
so when you use redirect you only need to pass in the view args and you can pickup template args when running that view


# Debugging views
pdb.set_trace()

# With pretty-print
pp( Employee.nodes.all() )

"""


#-------------------------- PSQL USER ---------------------------


def signup(request):
  if request.method == 'POST':
    form = SignUpForm(request.POST)
    if form.is_valid():
      user = form.save(commit=False)
      user.is_active = False
      user.save()
      current_site = get_current_site(request)
      subject = 'Activate Your MySite Account'
      message = render_to_string('account_activation_email.html', {
        'user': user,
        'domain': current_site.domain,
        'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
        'token': account_activation_token.make_token(user),
      })
      user.email_user(subject, message)
      return redirect('account_activation_sent')
  else:
      form = SignUpForm()
  return render(request, 'signup.html', {'form': form})




def account_activation_sent(request):
  return render(request, 'account_activation_sent.html')




def activate(request, uidb64, token):
  try:
    uid = urlsafe_base64_decode(uidb64).decode()
    user = User.objects.get(pk=uid)
  except (TypeError, ValueError, OverflowError, User.DoesNotExist):
    user = None

  if user is not None and account_activation_token.check_token(user, token):
    user.is_active = True
    user.profile.email_confirmed = True
    user.save()
    login(request, user)
    messages.success(request, 'Welcome to OrgDB! Your account was created successfully')
    return redirect('employee_list')
  else:
    return render(request, 'account_activation_invalid.html')




@receiver(user_logged_out)
def on_user_logged_out(sender, request, user, **kwargs):
  msg = 'You have securely logged out. Thank you for visiting.'
  messages.add_message(request, messages.INFO, msg)

def homepage(request):
  return render(request, 'homepage.html')
# if logged in then: settings.py `LOGIN_REDIRECT_URL = 'employee_list'`




#-------------------------- PSQL ORG ---------------------------


def organization_create(request):
  form = OrganizationForm(request.POST or None)

  if form.is_valid():
    form.save()
    return redirect('organization_list')

  return render(request, 'organization/create.html', {'form':form})




def organization_list(request):
  user = request.user
  user_orgs = list(user.profile.organizations.all())
  context = {
    "user": user,
    "user_orgs": user_orgs,
  }
  
  return render(request, 'organization/list.html', context)




def organization_detail(request, id):
  organization = Organization.objects.get(id=id)

  return render(request, 'organization/detail.html', {'organization': organization})




def organization_update(request, id):
  organization = Organization.objects.get(id=id)
  form = OrganizationForm(request.POST or None, instance=organization)

  if form.is_valid():
    form.save()
    return redirect('organization_detail', organization.id)

  return render(request, 'organization/update.html', {'form': form, 'organization': organization})




def organization_delete(request, id):
  organization = Organization.objects.get(id=id)

  if request.method == 'POST':
    organization.delete()
    return redirect('organization_list')

  return render(request, 'organization/delete.html', {'organization': organization})




#-------------------------- ORGCHART ---------------------------


def orgchart(request):
  return render(request, 'orgchart/orgchart.html')




# ajax that gets the graph data then serializes for visjs on the orgchart page
def need_orgchart_data(request):
  if request.method == 'GET':
    # in multi-tenant need to specify session org
    orgchart_graph_qstring = "MATCH (n) OPTIONAL MATCH (n)-[r:REPORTS_TO|BRANCH_OF|OVERSEEN_BY|BASED_IN]->() RETURN collect(DISTINCT n) as nodes, collect(r) as edges"
    # orgchart_graph_qstring = "MATCH (n)-[r:REPORTS_TO|BRANCH_OF|OVERSEEN_BY|BASED_IN]->() RETURN collect(DISTINCT n) as nodes, collect(r) as edges"
    # just returns employees... MATCH (n:Employee) OPTIONAL MATCH (n)-[r:REPORTS_TO|BRANCH_OF|OVERSEEN_BY]->() RETURN collect(DISTINCT n) as nodes, collect(r) as edges
    orgchart_graph_objects = db.cypher_query(orgchart_graph_qstring)
    """
      - for the meantime, I am accessing the nodes and edges based on index position[][][]. though, looking back, I forget why
      - the  node attribute `labels=` is not json serializable and the example uses `"label":` not `"labels":`
      - you can distinguish between different types of nodes and edges by their label
    """
    raw_nodes = orgchart_graph_objects[0][0][0]
    raw_edges = orgchart_graph_objects[0][0][1]
    
    # python lists to be converted into json arrays in json.dumps
    unsorted_nodes = []
    nodes_for_json = []
    edges_for_json = []

    for rn in raw_nodes:
      """ 
        - python dict converts to a json object in json.dumps
        - node labels are natural of object type 'set'
        - join the set with an empty string to make it a string type. this will only work if there is only 1 item in the labels set.
      """
      node = {
        "id":rn.id,
        "uid":rn.properties['uid'],
        "group":''.join(rn.labels), #take the neo4j `label` and use it as a visjs `group`
      }

      if node['group'] == "Employee":
        node['label'] = rn.properties['name_full']
        node['job_title'] = rn.properties['job_title']
        try:
          node['image'] = rn.properties['profile_picture']
        except:
          node['image'] = "https://s3.amazonaws.com/orgdb-assets/media/profile-icon-square.png"

      elif node['group'] == "Department":
        node['label'] = rn.properties['name_short']
        node['name_long'] = rn.properties['name_long']
      
      elif node['group'] == "Office":
        node['label'] = rn.properties['name_short']
        node['name_long'] = rn.properties['name_long']

      nodes_for_json.append(node)

    for re in raw_edges:
      # python dict convert to json object
      edge = {
          "id":re.id,
          # "label":re.type, # the graph just gets messy with edge labels. could set key to something else that shows on hover.
          "from":re.start,
          "to":re.end,
          #"properties": []
        }
      edges_for_json.append(edge)

    nodes_and_edges_for_json = json.dumps(
      {
        "nodes": nodes_for_json,
        "edges": edges_for_json
      },
      ensure_ascii = False
    )

    orgchart_data = json.loads(nodes_and_edges_for_json)

    response = {
      'orgchart_data': orgchart_data
    }

    return JsonResponse(response, safe=False)



# ajax that gets the employee data for side panel when graph node clicked
def need_node_data(request):
  if request.method == 'GET':
    node_group = request.GET['node_group']
    node_uid = request.GET['node_uid']

    if node_group == "Employee":
      employee = Employee.nodes.get(uid=node_uid)
      """
      if employee.bosses:
      for boss in bosses:
        i
        # create a key for bosses with string of "boss-1" dash?
        # hopefully can just ul through them
      response. = employee.bosses[i]
      """
      response = {
        'node_group': node_group,
        'uid': employee.uid,
        'pic': employee.profile_picture,
        'name': employee.name_full,
        'sub_name': employee.job_title,
        'description': employee.job_description,
        # rows for table
        'details': { 
          'Email': employee.email_work,
          'Slack': employee.username_slack,
          'Hired': employee.date_hired
        }
      }
    elif node_group == "Department":
      department = Department.nodes.get(uid=node_uid)
      response = {
        'node_group': node_group,
        'uid': department.uid,
        'pic': "static/images/department-icon.png",
        'name': department.name_short,
        'sub_name': department.name_long,
        'description': department.mission_statement,
        # rows for table
        'details': { 
          'Distribution': department.email_distribution,
          'Slack Channel': department.channel_slack,
        }
      }
    elif node_group == "Office":
      office = Office.nodes.get(uid=node_uid)
      response = {
        'node_group': node_group,
        'uid': office.uid,
        'pic': "static/images/office-icon.png",
        'name': office.name_short,
        'sub_name': office.name_long,
        # rows for table
        'details': { 
          'Distribution': office.email_distribution,
          'Slack Channel': office.channel_slack,
        }
      }

    # By default, the JsonResponse’s first parameter, data, should be a dict instance. To pass any other JSON-serializable object you must set the safe parameter to False.
    return JsonResponse(response, safe=False)




#-------------------------- NEO4J EMPLOYEE ---------------------------


def employee_create(request):
  #organization = Organization.objects.get(id=id)
  form = EmployeeForm(request.POST or None)

  if form.is_valid():
    instance = form.save()
    messages.success(request, 'Employee record created successfully!')
    return redirect("employee_detail", instance.uid)

  return render(request, 'employee/create.html', {'form':form})




def employee_list(request):
  user = request.user #so we can do silly things with their profile picture
  alphabet = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',]
  employees_by_letter = {}  
  employees = Employee.nodes.all()
  
  for employee in employees:
    employee.flat_name_last = unidecode(employee.name_last)

  for letter in alphabet: 
    # filtering lists -- http://www.diveintopython.net/power_of_introspection/filtering_lists.html    
    # compare the first letter of the name_last to the current iterated letter. note that the name_last property appears again in the filter key.
    employees_by_this_letter = [employee for employee in employees if employee.flat_name_last[0].upper() == letter]
    # set the letter as the key and the list of matching employees as the value
    employees_by_letter[letter.upper()] = employees_by_this_letter

  context = {
    "user": user,
    "employees_by_letter": employees_by_letter,
    "total_employees": len(employees),
  }

  return render(request, 'employee/list.html', context)




def employee_detail(request, uid):
  employee = Employee.nodes.get(uid=uid)
  
  return render(request, 'employee/detail.html', {'employee': employee,})




def employee_update(request, uid):
  employee = Employee.nodes.get(uid=uid)
  form = EmployeeForm(request.POST or None, instance=employee)
 
  if form.is_valid():
    form.save()
    messages.success(request, 'Employee record updated successfully!')
    return redirect("employee_detail", employee.uid)

  return render(request, 'employee/update.html', {'form': form, 'employee': employee})




# profile_picture, profile picture
# s3upload package -- https://github.com/yunojuno/django-s3-upload
# class FileCreate(FormView): template_name = 'profile-picture.html' form_class = S3UploadForm
def file_create(request, uid):
  employee = Employee.nodes.get(uid=uid)
  form = S3UploadForm(request.POST or None)
  
  return render(request, 'profile-picture.html', {'form': form, 'employee': employee})




def employee_delete(request, uid):
  employee = Employee.nodes.get(uid=uid)

  if request.method == 'POST':
    employee.delete()
    messages.warning(request, 'Employee record was deleted successfully!')
    return redirect('employee_list')

  return render(request, 'employee/delete.html', {'employee': employee})




def employee_boss_remove(request, uid, boss_uid, instruction):
  employee = Employee.nodes.get(uid=uid)
  boss = Employee.nodes.get(uid=boss_uid)
  
  employee.bosses.disconnect(boss)
  
  if instruction == "remove direct report":
    messages.warning(request, 'Team member was removed successfully!')
    return redirect("employee_detail", boss.uid)

  messages.warning(request, 'Manager was removed successfully!')
  return redirect("employee_detail", employee.uid)




def employee_boss_add(request, uid):
  employee = Employee.nodes.get(uid=uid)

  if request.method=='POST':
    form = EmployeeBossAddForm(request.POST, employee=employee)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      boss_email = form.cleaned_data['boss_email']
      boss = Employee.nodes.get(email_work=boss_email)

      drop_old_departments(request, form.data, employee)
      drop_old_bosses(request, form.data, employee)
      adopt_new_departments(request, form.data, boss, employee)
      employee.bosses.connect(boss)
      check_reverse_report_rel(request, form.data, employee, boss)

      messages.success(request, "Manager was added successfully!")
      return redirect("employee_detail", employee.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'employee/boss-add.html', {'form': form, 'employee': employee})

  # if method is not POST... GET
  return render(request, 'employee/boss-add.html', {'form': EmployeeBossAddForm, 'employee': employee})




def employee_direct_report_add(request, uid):
  boss = Employee.nodes.get(uid=uid)
  direct_reports = boss.direct_reports

  if request.method=='POST':
    form = EmployeeDirectReportAddForm(request.POST, employee=boss)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      direct_report_email = form.cleaned_data['direct_report_email']
      direct_report = Employee.nodes.get(email_work=direct_report_email)
      
      drop_old_departments(request, form.data, direct_report)
      drop_old_bosses(request, form.data, direct_report)
      adopt_new_departments(request, form.data, boss, direct_report)
      direct_report.bosses.connect(boss)   
      check_reverse_report_rel(request, form.data, direct_report, boss)

      messages.success(request, "New member was added to the team successfully!")
      return redirect("employee_detail", boss.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'employee/direct-report-add.html', {'form': form, 'employee': boss,})
  
  # if method is not POST... GET request to render the regular form
  return render(request, 'employee/direct-report-add.html', {'form': EmployeeDirectReportAddForm, 'employee': boss,})




def employee_update_profile_pic (request, uid):
  if request.method == "POST":
    employee = Employee.nodes.get(uid=uid)
    new_profile_pic = request.POST['s3_link']
    employee.profile_picture = new_profile_pic
    employee.save()

    messages.success(request, """Successfully saved new profile pic! Lookin' good!""")
    return redirect("employee_detail", employee.uid)




#-------------------------- NEO4J DEPARTMENT ---------------------------


def department_list(request):
  departments = Department.nodes.order_by('name_short').all()

  # some quick stats
  dept_sizes = []
  for dept in departments:
    # counts
    num_branches = len( dept.branches.all() )
    dept.num_branches = num_branches

    num_members = len( dept.members.all() )
    dept.num_members = num_members
    dept_sizes.append(dept.num_members)
    
  med_dept_size = int( numpy.median(dept_sizes) )
  avg_dept_dize = int( numpy.mean(dept_sizes) )

  context = {
    "departments": departments,
    "med_dept_size": med_dept_size,
    "avg_dept_dize": avg_dept_dize,
    "total_departments": len(departments),
  }

  return render(request, 'department/list.html', context)




def department_create(request):
  form = DepartmentForm(request.POST or None)

  if form.is_valid():
    instance = form.save()
    messages.success(request, 'New department created successfully!')
    return redirect("department_detail", instance.uid)

  return render(request, 'department/create.html', {'form':form})




def department_detail(request, uid):
  department = Department.nodes.get(uid=uid)

  members_with_manager_tags = check_if_any_members_are_managers(department)
  return render(request, 'department/detail.html', {'department': department, 'members_with_manager_tags': members_with_manager_tags,})




def department_update(request, uid):
  department = Department.nodes.get(uid=uid)
  form = DepartmentForm(request.POST or None, instance=department)

  if form.is_valid():
    form.save()
    messages.success(request, 'Department information was updated successfully!')
    return redirect("department_detail", department.uid)

  members_with_manager_tags = check_if_any_members_are_managers(department)
  return render(request, 'department/update.html', {'form': form, 'department': department, 'members_with_manager_tags': members_with_manager_tags,})




# this method is currently not in use and its path() is comment out. see `department_merge_delete`.
def department_merge_delete(request, uid):
  department = Department.nodes.get(uid=uid)

  if request.method == 'POST':
    department.delete()
    messages.warning(request, 'Department grouping was deleted successfully, but any employees are still here!')
    return redirect('department_list')

  return render(request, 'department/delete.html', {'department': department})




# this is a department adding a member. which uses employee email.
def department_employee_add(request, uid):
  department = Department.nodes.get(uid=uid)
  # this is not used for logic, but is passed back to detail views
  members_with_manager_tags = check_if_any_members_are_managers(department)

  if request.method=='POST':
    form = DepartmentEmployeeAdd(request.POST, department=department)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      member_email = form.cleaned_data['member_email']
      member = Employee.nodes.get(email_work=member_email)

      drop_old_departments(request, form.data, member)
      member.departments.connect(department)
      check_should_be_department_manager_and_connect(request, form.data, member, department)

      return redirect("department_detail", department.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'department/member-add.html', {'form': form, 'department': department, 'members_with_manager_tags': members_with_manager_tags,})

  # if method is not POST... GET
  return render(request, 'department/member-add.html', {'form': DepartmentEmployeeAdd, 'department': department, 'members_with_manager_tags': members_with_manager_tags,})




# this is an employee joining a department. which uses department alias.
def employee_department_add(request, uid):
  employee = Employee.nodes.get(uid=uid)

  if request.method=='POST':
    form = EmployeeDepartmentAddForm(request.POST, employee=employee)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      dept_alias = form.cleaned_data['dept_alias']
      department = Department.nodes.get(name_short=dept_alias)

      drop_old_departments(request, form.data, employee)
      employee.departments.connect(department)
      check_should_be_department_manager_and_connect(request, form.data, employee, department)

      return redirect("employee_detail", employee.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'employee/department-add.html', {'form': form, 'employee': employee,})

  # if method is not POST... GET
  return render(request, 'employee/department-add.html', {'form': EmployeeDepartmentAddForm, 'employee': employee,})




def department_employee_remove(request, department_uid, member_uid, instruction):
  department = Department.nodes.get(uid=department_uid)
  member = Employee.nodes.get(uid=member_uid)
  
  member.departments.disconnect(department)
  
  if instruction == "remove department manager":
    member.departments_managed.disconnect(department)
    messages.warning(request, 'Department manager was deposed successfully! With a successfull coup, chaos ensues.')
    return redirect("department_detail", department.uid)
  # this is when you remove an employee from the department from the employee_update page ;) as opposed to the department_update page
  elif instruction == "remove membership":
    member.departments_managed.disconnect(department)
    messages.warning(request, """Removed employee from department successfully! Don't worry, they still exist.""")
    return redirect("employee_detail", member.uid)

  messages.warning(request, """Team member was removed from department successfully! Don't worry, they still exist.""")
  return redirect("department_detail", department.uid)




"""
I want to get rid of this function by checking the bool property in a view, but
-- tried .is_connected() which uses () which is a no go in {% %}
-- tried templatetags which use {{}} which is a no go in {% %} 
----- and even if you try to get all of the html into a returned string then special characters in that string break the string
-- tried for member in department.members: if member in department.overseers... somehow the 2nd statement evaluates false
$ reflecting on this months later, it's not bad to have for an api... because
the MEMBER_OF and OVERSEEN_BY are diff rels 
"""
def department_manager_set(request, department_uid, member_uid, instruction):
  department = Department.nodes.get(uid=department_uid)
  member = Employee.nodes.get(uid=member_uid)
  rel = member.departments.relationship(department)
  
  if instruction == "remove as manager":
    rel.is_manager = False
    rel.save()
    member.departments_managed.disconnect(department)

    messages.warning(request, 'Department manager deposed successfully! With a successfull coup, chaos shall ensue.')
    return redirect("department_detail", department.uid)
  
  elif instruction == "promote to manager":
    rel.is_manager = True
    rel.save()
    member.departments_managed.connect(department)

    messages.success(request, 'The lowly associate looked up toward the light and stepped into the managerial role. Be sure to congratulate them.')
    return redirect("department_detail", department.uid)




def department_parent_add(request, uid):
  child_department = Department.nodes.get(uid=uid)

  if request.method=='POST':
    form = DepartmentParentAddForm(request.POST, department=child_department)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      parent_alias = form.cleaned_data['parent_department_alias']
      parent_department = Department.nodes.get(name_short=parent_alias)

      # finally, add the parent
      child_department.parent_departments.connect(parent_department)
      
      # check if there was already a reverse relationship
      if parent_department.parent_departments.is_connected(child_department) == True:
        messages.warning(request, "It looks like you just made two departments roll up to each other. While this is fine, we just wanted to give you a heads up in case you accidentally told an executive team that they report to middle management ;)")

      messages.success(request, "Parent department was added successfully!")
      return redirect("department_detail", child_department.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'department/parent-add.html', {'form': form, 'department': child_department})

  # if method is not POST... GET
  return render(request, 'department/parent-add.html', {'form': DepartmentParentAddForm, 'department': child_department})




def department_parent_remove(request, uid, parent_uid, instruction):
  department = Department.nodes.get(uid=uid)
  parent = Department.nodes.get(uid=parent_uid)
  
  department.parent_departments.disconnect(parent)
  
  if instruction == "remove branch":
    messages.warning(request, 'Branch department was removed successfully!')
    return redirect("department_detail", parent.uid)

  messages.warning(request, 'Parent department was removed successfully!')
  return redirect("department_detail", department.uid)




def department_branch_add(request, uid):
  parent_department = Department.nodes.get(uid=uid)

  if request.method=='POST':
    form = DepartmentBranchAdd(request.POST, department=parent_department)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      child_alias = form.cleaned_data['branch_department_alias']
      child_department = Department.nodes.get(name_short=child_alias)

      # drop old departments. this must come before the add.
      drop_old_parents(request, form.data, child_department)

      # finally, add the branch
      parent_department.branches.connect(child_department)
      
      # check if there was already a reverse relationship
      if child_department.branches.is_connected(parent_department) == True:
        messages.warning(request, "It looks like you just made two departments roll up to each other. While this is fine, we just wanted to give you a heads up in case you accidentally told an executive team that they report to middle management ;)")

      messages.success(request, "Branch department was added successfully!")
      return redirect("department_detail", parent_department.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'department/branch-add.html', {'form': form, 'department': parent_department})

  # if method is not POST... GET
  return render(request, 'department/branch-add.html', {'form': DepartmentBranchAdd, 'department': parent_department})



# merges the members of a department into another department before deleting the original department
def department_merge_delete(request, uid):
  dept_drop = Department.nodes.get(uid=uid)

  if request.method=='POST':
    form = DeptMergeDeleteForm(request.POST, department=dept_drop)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():

      # check to see if they want to merge their department members into another department before deleting this department
      try:
        into_alias = form.cleaned_data['into_alias']
      except:
        into_alias = None
      
      if into_alias is not '' or None:
        dept_into = Department.nodes.get(name_short=into_alias)

        for member in dept_drop.members:
          # delete all of the old manager flags
          if member.departments_managed.is_connected(dept_drop) is True:
            member.departments_managed.disconnect(dept_drop)

          # only connect the people that are not already members of dept_into
          if member.departments.is_connected(dept_into) == False:
            member.departments.connect(dept_into)

        dept_drop.delete()
        messages.success(request, "Successfully deleted the old department and merged all of its members into this department!")

        members_with_manager_tags = check_if_any_members_are_managers(dept_into)
        return render(request, 'department/detail.html', {'department': dept_into, 'members_with_manager_tags': members_with_manager_tags,})

      dept_drop.delete()
      messages.warning(request, "Successfully deleted department. Please make sure you add any floating employees to a new department.")
      return redirect('department_list')

    members_with_manager_tags = check_if_any_members_are_managers(dept_drop)
    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'department/merge-delete.html', {'form': form, 'department': dept_drop, 'members_with_manager_tags': members_with_manager_tags,})

  members_with_manager_tags = check_if_any_members_are_managers(dept_drop)
  # if method is not POST... GET
  return render(request, 'department/merge-delete.html', {'form': DeptMergeDeleteForm, 'department': dept_drop, 'members_with_manager_tags': members_with_manager_tags,})




#-------------------------- NEO4J OFFICE ---------------------------

def office_list(request):
  offices = Office.nodes.order_by('name_short').all()

  return render(request, 'office/list.html', {'offices': offices})




def office_create(request):
  form = OfficeForm(request.POST or None)

  if form.is_valid():
    instance = form.save()
    messages.success(request, 'New office created successfully!')
    return redirect("office_detail", instance.uid)

  return render(request, 'office/create.html', {'form':form})




def office_detail(request, uid):
  office = Office.nodes.get(uid=uid)

  inhabitants_with_manager_tags = check_if_any_inhabitants_are_managers(office)
  return render(request, 'office/detail.html', {'office': office, 'inhabitants_with_manager_tags': inhabitants_with_manager_tags})




def office_update(request, uid):
  office = Office.nodes.get(uid=uid)
  form = OfficeForm(request.POST or None, instance=office)

  if form.is_valid():
    form.save()
    messages.success(request, 'Office information was updated successfully!')
    return redirect("office_detail", office.uid)

  inhabitants_with_manager_tags = check_if_any_inhabitants_are_managers(office)
  return render(request, 'office/update.html', {'form': form, 'office': office, 'inhabitants_with_manager_tags': inhabitants_with_manager_tags})




def office_delete(request, uid):
  office = Office.nodes.get(uid=uid)

  if request.method == 'POST':
    office.delete()
    messages.warning(request, 'Office record was deleted successfully!')
    return redirect('office_list')

  return render(request, 'office/delete.html', {'office': office})




# this is an office adding an employee. which uses employee email.
def office_employee_add(request, uid):
  office = Office.nodes.get(uid=uid)
  # this is not used for logic, but is passed back to detail views
  inhabitants_with_manager_tags = check_if_any_inhabitants_are_managers(office)

  if request.method=='POST':
    form = OfficeEmployeeAddForm(request.POST, office=office)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors.
    # However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      employee_email = form.cleaned_data['employee_email']
      employee = Employee.nodes.get(email_work=employee_email)

      employee_drop_old_offices(request, form.data, employee)
      employee.offices.connect(office)
      check_should_be_office_manager_and_connect(request, form.data, employee, office)

      return redirect("office_detail", office.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'office/employee-add.html', {'form': form, 'office': office})#, 'members_with_manager_tags': members_with_manager_tags,})

  # if method is not POST... GET
  return render(request, 'office/employee-add.html', {'form': OfficeEmployeeAddForm, 'office': office})#, 'members_with_manager_tags': members_with_manager_tags,})




# this is an employee joining an office. which uses office alias.
def employee_office_add(request, uid):
  employee = Employee.nodes.get(uid=uid)

  if request.method=='POST':
    form = EmployeeOfficeAddForm(request.POST, employee=employee)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      office_alias = form.cleaned_data['office_alias']
      office = Office.nodes.get(name_short=office_alias)

      employee_drop_old_offices(request, form.data, employee)
      employee.offices.connect(office)
      check_should_be_office_manager_and_connect(request, form.data, employee, office)

      return redirect("employee_detail", employee.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'employee/office-add.html', {'form': form, 'employee': employee,})

  # if method is not POST... GET
  return render(request, 'employee/office-add.html', {'form': EmployeeOfficeAddForm, 'employee': employee,})




def office_employee_remove(request, office_uid, employee_uid, instruction):
  office = Office.nodes.get(uid=office_uid)
  employee = Employee.nodes.get(uid=employee_uid)
  
  employee.offices.disconnect(office)
  
  if instruction == "remove office manager":
    employee.offices_managed.disconnect(office)
    messages.warning(request, 'Office manager was deposed successfully! With a successfull coup, chaos ensues.')
    return redirect("office_detail", office.uid)
  # this is when you remove an employee from the office from the employee_update page ;) as opposed to the office_update page
  elif instruction == "remove employee":
    employee.offices_managed.disconnect(office)
    messages.warning(request, """Removed employee from office successfully! Don't worry, they still exist.""")
    return redirect("employee_detail", employee.uid)

  messages.warning(request, """Employee was removed from the office successfully! Don't worry, they still exist.""")
  return redirect("office_detail", office.uid)




def office_manager_set(request, office_uid, employee_uid, instruction):
  office = Office.nodes.get(uid=office_uid)
  employee = Employee.nodes.get(uid=employee_uid)
  rel = employee.offices.relationship(office)
  
  if instruction == "remove as office manager":
    rel.is_manager = False
    rel.save()
    employee.offices_managed.disconnect(office)

    messages.warning(request, 'Office manager deposed successfully! With a successfull coup, chaos shall ensue.')
    return redirect("office_detail", office.uid)
  
  elif instruction == "promote to office manager":
    rel.is_manager = True
    rel.save()
    employee.offices_managed.connect(office)

    messages.success(request, 'The lowly associate looked up toward the light and stepped into the managerial role. Be sure to congratulate them.')
    return redirect("office_detail", office.uid)




# merges the members and departments into another office before deleting the original office
def office_merge_delete(request, uid):
  office_drop = Office.nodes.get(uid=uid)

  if request.method=='POST':
    form = OfficeMergeDeleteForm(request.POST, office=office_drop)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors. However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():

      # check to see if they want to merge their department members into another department before deleting this department
      try:
        into_alias = form.cleaned_data['into_alias']
      except:
        into_alias = None
      
      if into_alias is not '' or None:
        office_into = Office.nodes.get(name_short=into_alias)

        # don't need to call drop_old_offices because office_drop.delete
        # deletes all of the the old relationships as well

        for employee in office_drop.employees:
          # only connect the people that are not already members of office_into
          if employee.offices.is_connected(office_into) == False:
            employee.offices.connect(office_into)


        for department in office_drop.departments:
          # only connect the departments that are not already part of office_into
          if department.offices.is_connected(office_into) == False:
            department.offices.connect(office_into)

        office_drop.delete()
        messages.success(request, "Successfully deleted the old office and merged all of its employees and departments into this office!")

        inhabitants_with_manager_tags = check_if_any_inhabitants_are_managers(office_into)
        return render(request, 'office/detail.html', {'office': office_into, 'inhabitants_with_manager_tags': inhabitants_with_manager_tags,})

      office_drop.delete()
      messages.warning(request, "Successfully deleted office. Please make sure you add any floating employees to a new office.")
      return redirect('office_list')

    inhabitants_with_manager_tags = check_if_any_inhabitants_are_managers(office_drop)
    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'office/merge-delete.html', {'form': form, 'office': office_drop, 'inhabitants_with_manager_tags': inhabitants_with_manager_tags,})

  inhabitants_with_manager_tags = check_if_any_inhabitants_are_managers(office_drop)
  # if method is not POST... GET
  return render(request, 'office/merge-delete.html', {'form': OfficeMergeDeleteForm, 'office': office_drop, 'inhabitants_with_manager_tags': inhabitants_with_manager_tags,})




# this is an office adding a department. which uses department alias.
def office_department_add(request, uid):
  office = Office.nodes.get(uid=uid)

  if request.method=='POST':
    form = OfficeDepartmentAddForm(request.POST, office=office)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors.
    # However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      department_alias = form.cleaned_data['department_alias']
      department = Department.nodes.get(name_short=department_alias)

      department_drop_old_offices(request, form.data, department)
      merge_employees(request, form.data, department, office)

      office.departments.connect(department)
      messages.success(request, "Successfully added new department to the office!")

      return redirect("office_detail", office.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'office/department-add.html', {'form': form, 'office': office})

  # if method is not POST... GET
  return render(request, 'office/department-add.html', {'form': OfficeDepartmentAddForm, 'office': office})




# this is a department adding an office. which uses office alias.
def department_office_add(request, uid):
  department = Department.nodes.get(uid=uid)

  if request.method=='POST':
    form = DepartmentOfficeAddForm(request.POST, department=department)

    # form.is_valid() is what annotates the existing form with error information in the case where there are errrors.
    # However, if form.is_valid() returns False, the code here immediately overwrites the existing (now annotated with error information) form and replaces it with a newly-created blank form, so there will be no errors for the template to display. dont use an else
    if form.is_valid():
      office_alias = form.cleaned_data['office_alias']
      office = Office.nodes.get(name_short=office_alias)

      department_drop_old_offices(request, form.data, department)
      merge_employees(request, form.data, department, office)

      office.departments.connect(department)
      messages.success(request, "Successfully added department to the new office!")

      return redirect("department_detail", department.uid)

    # if form is not valid. errors are now attached to the form object and will auto-appear in the template as <ul class="errorlist"> <li>'s
    return render(request, 'department/office-add.html', {'form': form, 'department': department})

  # if method is not POST... GET
  return render(request, 'department/office-add.html', {'form': DepartmentOfficeAddForm, 'department': department})




def office_department_remove(request, uid, department_uid, instruction):
  office = Office.nodes.get(uid=uid)
  department = Department.nodes.get(uid=department_uid)
  
  office.departments.disconnect(department)
  
  if instruction == "remove office":
    messages.warning(request, 'Office was left successfully!')
    return redirect("department_detail", department.uid)

  messages.warning(request, 'Department was disconnected successfully!')
  return redirect("office_detail", office.uid)



#-------------------------- DRY FUNCTIONS ---------------------------

"""
- despite the DepartmentManagerRel, you cannot check is_connected() in the view, so this is necessary. see `department_manager_set` comment for more detail
- however, it is somewhat efficient in that it only checks for members of the department in question, rather than all employees
- the page also does not have to wait for an ajax call
"""
def check_if_any_members_are_managers(department):
  members_with_manager_tags = []

  department_members = department.members
  for member in department_members:
    if member.departments_managed.is_connected(department) is True:
      member.is_manager = True
    members_with_manager_tags.append(member)

  return members_with_manager_tags




def check_if_any_inhabitants_are_managers(office):
  inhabitants_with_manager_tags = []

  office_inhabitants = office.employees
  for emp in office_inhabitants:
    if emp.offices_managed.is_connected(office) is True:
      emp.is_manager = True
    inhabitants_with_manager_tags.append(emp)

  return inhabitants_with_manager_tags




def drop_old_departments(request, form_data, employee):
  try:
    drop_old_departments = form_data['drop_old_departments']
  except:
    drop_old_departments = False
  if drop_old_departments == 'on':
    for old_dept in employee.departments:
      employee.departments.disconnect(old_dept)
      if employee.departments_managed.is_connected(old_dept) is True:
        employee.departments_managed.disconnect(old_dept)
    messages.warning(request, "Disconnected employee from any previous departments!")




def drop_old_bosses(request, form_data, employee):
  try:
    drop_old_bosses = form_data['drop_old_bosses']
  except:
    drop_old_bosses = False
  if drop_old_bosses == 'on':
    for old_boss in employee.bosses:
      employee.bosses.disconnect(old_boss)
    messages.warning(request, "Disconnected employee from any previous bosses!")




def drop_old_parents(request, form_data, child_department):
  try:
    drop_old_parents = form_data['drop_old_parents']
  except:
    drop_old_parents = False
  if drop_old_parents == 'on':
    for parent in child_department.parent_departments:
      child_department.parent_departments.disconnect(parent)
    messages.warning(request, "Disconnected department from any previous parent departments!")




def adopt_new_departments(request, form_data, boss, employee):
  try:
    adopt_new_departments = form_data['adopt_new_departments']
  except:
    adopt_new_departments = False

  if adopt_new_departments == 'on':
    for department in boss.departments:
      employee.departments.connect(department)
    messages.success(request, "Added employee to each of their new manager's departments!")




def check_should_be_department_manager_and_connect(request, form_data, employee, department):
  # you should technically be a member of a department before you are a manager of it
  try:
    is_manager = form_data['is_manager']
  except:
    is_manager = False

  if is_manager == 'on':
    employee.departments_managed.connect(department)
    messages.success(request, "New manager was successfully added to the department!")
  else:
    messages.success(request, "New team member was added to department successfully!")




def check_should_be_office_manager_and_connect(request, form_data, employee, office):
  # you should technically be a member of a department before you are a manager of it
  try:
    is_manager = form_data['is_manager']
  except:
    is_manager = False

  if is_manager == 'on':
    employee.offices_managed.connect(office)
    messages.success(request, "New manager was successfully added to the office!")
  else:
    messages.success(request, "New employee was added to office successfully!")




def check_reverse_report_rel(request, form_data, employee, boss):
  # check if there was already a reverse relationship
  if boss.bosses.is_connected(employee) == True:
    messages.warning(request, "It looks like you just made two employees report directly to each other. While this is fine, we just wanted to give you a heads up in case you accidentally told a manager that their employee is now their boss. ;)")




def employee_drop_old_offices(request, form_data, employee):
  try:
    drop_old_offices = form_data['drop_old_offices']
  except:
    drop_old_offices = False
  if drop_old_offices == 'on':
    for old_office in employee.offices:
      employee.offices.disconnect(old_office)
      if employee.offices_managed.is_connected(old_office) is True:
        employee.offices_managed.disconnect(old_office)
    messages.warning(request, "Disconnected employee from any previous offices!")




def department_drop_old_offices(request, form_data, department):
  try:
    drop_old_offices = form_data['drop_old_offices']
  except:
    drop_old_offices = False
  if drop_old_offices == 'on':
    for old_office in department.offices:
      department.offices.disconnect(old_office)
    messages.warning(request, "Disconnected department from any previous offices!")




def merge_employees(request, form_data, department, office):
  try:
    merge_employees = form_data['merge_employees']
  except:
    merge_employees = False
  if merge_employees == 'on':
    for employee in department.members:
      for old_office in employee.offices:
        employee.offices.disconnect(old_office)
        if employee.offices_managed.is_connected(old_office) is True:
          employee.offices_managed.disconnect(old_office)
      employee.offices.connect(office)
    messages.warning(request, "Disconnected employees from any previous offices!")
    messages.success(request, "Connected employees to the new office!")
