from django import template
from django.utils.safestring import mark_safe


register = template.Library()

"""
@register.filter
def my_filter(value, arg):
  if value > arg is True:
    return some_value or mark_safe(my_html_string)
"""

# templatetags -- https://www.youtube.com/watch?v=P_wyVIUl4P4
# pass it into the template as {{member|check_is_manager}}
# pass in with an arg {{member|check_is_manager:department}}
# there are django filters that let you do {{value|some_filter}}
# you can string them together too {{value|some_filter|another_filter}}
# therefore this is a custom filter
