from django.urls import include, path
from django.contrib.auth import views as auth_views
from django.template.loader import get_template
from . import views


urlpatterns = [
  path('', views.homepage, name='homepage'),
  
  # ------------------------ VIEWS ------------------------
  path('organizations', views.organization_list, name='organization_list'),
  path('organization/new/', views.organization_create, name='organization_create'),
  path('organization/detail/<int:id>', views.organization_detail, name='organization_detail'),
  path('organization/update/<int:id>', views.organization_update, name='organization_update'),
  path('organization/delete/<int:id>', views.organization_delete, name='organization_delete'),
  
  path('orgchart', views.orgchart, name='orgchart'),
  # these should be dashes
  path('need-node-data', views.need_node_data, name='need_node_data'), # ajax call made by the sidebar
  path('need-orgchart-data', views.need_orgchart_data, name='need_orgchart_data'), # initial ajax call made by the orgchart

  path('employees', views.employee_list, name='employee_list'),
  path('employee/new', views.employee_create, name='employee_create'),
  path('employee/detail/<str:uid>/', views.employee_detail, name='employee_detail'),
  path('employee/update/<str:uid>', views.employee_update, name='employee_update'),
  path('employee/update/<str:uid>/profile-picture', views.file_create, name='file_create'),
  path('employee/update/<str:uid>/profile-picture/save', views.employee_update_profile_pic, name='employee_update_profile_pic'),
  path('employee/delete/<str:uid>', views.employee_delete, name='employee_delete'),
  
  path('employee/<str:uid>/boss/new', views.employee_boss_add, name='employee_boss_add'),
  path('employee/<str:uid>/boss/delete/<str:boss_uid>/<str:instruction>', views.employee_boss_remove, name='employee_boss_remove'),  
  path('employee/<str:uid>/direct-report/new', views.employee_direct_report_add, name='employee_direct_report_add'),

  path('employee/<str:uid>/department/new', views.employee_department_add, name='employee_department_add'),
  path('employee/<str:uid>/office/new', views.employee_office_add, name='employee_office_add'),


  path('departments', views.department_list, name='department_list'),
  path('department/new', views.department_create, name='department_create'),
  path('department/detail/<str:uid>', views.department_detail, name='department_detail'),
  path('department/update/<str:uid>', views.department_update, name='department_update'),
  # when deleting a department, you merge its members to an optional other dept first
  path('department/<str:uid>/merge/delete', views.department_merge_delete, name='department_merge_delete'),

  path('department/<str:uid>/parent/new', views.department_parent_add, name='department_parent_add'),
  path('department/<str:uid>/parent/delete/<str:parent_uid>/<str:instruction>', views.department_parent_remove, name='department_parent_remove'),
  path('department/<str:uid>/branch/new', views.department_branch_add, name='department_branch_add'),

  path('department/<str:uid>/member/new', views.department_employee_add, name='department_employee_add'),
  path('department/<str:department_uid>/member/delete/<str:member_uid>/<str:instruction>', views.department_employee_remove, name='department_employee_remove'),  
  path('department/<str:department_uid>/manager/update/<str:member_uid>/<str:instruction>', views.department_manager_set, name='department_manager_set'),  

  path('department/<str:uid>/office/new', views.department_office_add, name='department_office_add'),


  path('offices', views.office_list, name='office_list'),
  path('office/new', views.office_create, name='office_create'),
  path('office/detail/<str:uid>', views.office_detail, name='office_detail'),
  path('office/update/<str:uid>', views.office_update, name='office_update'),
  # when deleting an office, you merge its employees and departments into an optional other office first
  path('office/<str:uid>/merge/delete', views.office_merge_delete, name='office_merge_delete'),

  path('office/<str:uid>/employee/new', views.office_employee_add, name='office_employee_add'),
  path('office/<str:office_uid>/manager/update/<str:employee_uid>/<str:instruction>', views.office_manager_set, name='office_manager_set'),  
  path('office/<str:office_uid>/employee/delete/<str:employee_uid>/<str:instruction>', views.office_employee_remove, name='office_employee_remove'),  

  path('office/<str:uid>/department/new', views.office_department_add, name='office_department_add'),
  path('office/<str:uid>/department/delete/<str:department_uid>/<str:instruction>', views.office_department_remove, name='office_department_remove'),


  # ------------------------ AUTH ------------------------
  path('login/', auth_views.login, name='login'),
  path('logout', auth_views.logout, {'next_page': auth_views.login}, name='logout'),
  path('register', views.signup, name='signup'),
  path('signup', views.signup, name='signup'),

  path('profile/password/reset', auth_views.password_reset, name='password_reset'),
  path('profile/password/reset/done', auth_views.password_reset_done, name='password_reset_done'),
  path('profile/password/reset/confirm/<uidb64>/<token>', auth_views.password_reset_confirm, name='password_reset_confirm'),
  path('profile/password/reset/complete', auth_views.password_reset_complete, name='password_reset_complete'),
  path('profile/password/change/', auth_views.password_change, name='password_change'),
  path('profile/password/change/done', auth_views.password_change_done, name='password_change_done'),

  path('account_activation_sent', views.account_activation_sent, name='account_activation_sent'),
  path('activate/<uidb64>/<token>', views.activate, name='activate'),
]
