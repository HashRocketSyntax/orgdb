from django import forms
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _
from django.core.exceptions import ObjectDoesNotExist
from django.template.defaultfilters import mark_safe
from django.core.validators import RegexValidator
from django.forms import Form, ModelForm, Textarea

from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import get_user_model
from .models import *
import pdb

from s3upload.widgets import S3UploadWidget




#-------------------------- DRY VALIDATOR FUNCTIONS ---------------------------
# forms.py doesn't let you define these functions at the bottom like in views.py. i think it's due to `class` 
User = get_user_model()

valid_email = RegexValidator(
    """(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])""",
    "Email Addresses must be formatted like so: president@whitehouse.gov"
  )


def valid_email_lower(value):
  if value.islower() == False :
    raise ValidationError("Sorry, all OrgDB emails must be lowercase.")

def valid_employee_email_exists(value):
  try:
    employee = Employee.nodes.get(email_work=value)
  except Employee.DoesNotExist:
    raise ValidationError("Sorry, we cannot find an employee with that email address.")

valid_dept_alias_chars = RegexValidator(
  '^(?!.*--)(?!.*__)(?!.*-_)(?!.*_-)([@]+[a-z][a-z\-_]+[a-z]+)$', 
  "Department Alias must: start with @ and the remainder can contain only lowercase letters a-z _underscores -dashes with neither trailing nor back-to-back special characters and spaces are right out!"
)

valid_office_alias_chars = RegexValidator(
  '^(?!.*--)(?!.*__)(?!.*-_)(?!.*_-)([&]+[a-z][a-z\-_]+[a-z]+)$', 
  "Office Alias must: start with & and the remainder can contain only lowercase letters a-z _underscores -dashes with neither trailing nor back-to-back special characters and spaces are right out!"
)

def valid_dept_alias_exists(value):
  try:
    department = Department.nodes.get(name_short=value)
  except Department.DoesNotExist:
    department = None
    raise ValidationError("Sorry, we cannot find a department with that alias.")

def valid_office_alias_exists(value):
  try:
    office = Office.nodes.get(name_short=value)
  except Office.DoesNotExist:
    office = None
    raise ValidationError("Sorry, we cannot find an office with that alias.")




#-------------------------- DRY FIELDS ---------------------------

class EmployeeDropOldOffices(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', True)
    kwargs.setdefault('label', mark_safe("""<i>In joining this office, the employee leave all of their old offices?</i>"""))
    super().__init__(*args, **kwargs)


class DepartmentDropOldOffices(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', True)
    kwargs.setdefault('label', mark_safe("""<i>In joining this office, the department leave all of its old offices?</i>"""))
    super().__init__(*args, **kwargs)


class DropOldDepartments(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', True)
    kwargs.setdefault('label', mark_safe("""<i>Should the employee leave their old departments?</i>"""))
    super().__init__(*args, **kwargs)


class DropOldBosses(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', True)
    kwargs.setdefault('label', mark_safe("""<i>Should the employee leave their old boss?</i>"""))
    super().__init__(*args, **kwargs)


class DropOldParents(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', False)
    kwargs.setdefault('label', mark_safe("""<i>When rolling up to this new parent department, should we break away from our existing parent departments? E.g. when the Louisianna Purchase left French control when it was acquired buy the United States.</i>"""))
    super().__init__(*args, **kwargs)


class AdoptNewDepartments(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', True)
    kwargs.setdefault('label', mark_safe("""<i>Should the employee join their new manager's departments if they have not done so already?</i>"""))
    super().__init__(*args, **kwargs)

class DepartmentManager(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', False)
    kwargs.setdefault('label', mark_safe("""<i>Will this employee be considered the department manager?</i>"""))
    super().__init__(*args, **kwargs)

class OfficeManager(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', False)
    kwargs.setdefault('label', mark_safe("""<i>Will this employee be considered the office manager?</i>"""))
    super().__init__(*args, **kwargs)

class OfficeMergeEmployees(forms.BooleanField):
  def __init__(self, *args, **kwargs):
    kwargs.setdefault('required', False)
    kwargs.setdefault('initial', True)
    kwargs.setdefault('label', mark_safe("""<i>In joining this office, should employees of this department leave their old office and join this one?</i>"""))
    super().__init__(*args, **kwargs)


#------------------------ PSQL ------------------------ 

class SignUpForm(UserCreationForm):
  first_name = forms.CharField(max_length=30)
  last_name = forms.CharField(max_length=50)
  email = forms.EmailField(max_length=254)

  class Meta:
    model = User
    fields = ('first_name', 'last_name', 'email', 'password1', 'password2',)




class OrganizationForm(forms.ModelForm):
  class Meta:
    model = Organization
    fields =['name', 'domain']




#------------------------ EMPLOYEES ------------------------ 

class EmployeeForm(forms.ModelForm):
  class Meta:
    model = Employee
    fields = [
      'email_work',
      'name_prefix', 'name_first', 'name_prefer', 'name_middle', 'name_last', 
      'name_is_prefer_used', 'name_is_prefix_used', 'name_is_middle_used',
      'job_title', 'job_description', 'profile_picture', 'username_slack',
      'date_hired',
    ]# 'phone_cell_work', 'phone_desk_work', 'skill_1', 'skill_1_rating', 'skill_2', 'skill_2_rating', 'skill_3', 'skill_3_rating', 'skill_4', 'skill_4_rating', 'skill_5', 'skill_5_rating']

  def clean_email_work(self):
    email_work = self.cleaned_data['email_work']
    valid_email_lower(email_work)

    return email_work




# s3upload package -- https://github.com/yunojuno/django-s3-upload
"""
- S3Upload is not the model name, File is.
  It was creating a table, but not writing to it. So added the meta.
- This form handles only the uploading to S3. 
  There's a separate form in html for saving s3 url to employee profile_picture
"""
class S3UploadForm(forms.Form):
  profile_pic = forms.URLField( widget=S3UploadWidget( dest='media_folder' ) )
  
  class Meta:
    create_db_table = False




#------------------------ BOSSES ------------------------ 

class EmployeeBossAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.employee = kwargs.pop('employee', None)
    super(EmployeeBossAddForm, self).__init__(*args, **kwargs)

  boss_email = forms.EmailField(
    required = True,
    max_length = 70,
    label = mark_safe("""<i>To give this employee a new manager, enter the manager's email address</i>"""),
    validators=[valid_email_lower, valid_employee_email_exists],
  )

  adopt_new_departments = AdoptNewDepartments()
  drop_old_departments = DropOldDepartments()
  drop_old_bosses = DropOldBosses()

  def clean_boss_email(self):
    employee = self.employee
    data = self.cleaned_data['boss_email']
    boss = Employee.nodes.get(email_work=data)

    if employee.bosses.is_connected(boss) == True:
      raise ValidationError("Sorry, that employee already reports to that manager. Cannot create duplicate relationship.")

    if boss == employee:
      raise ValidationError("Let's be real, we can't manage ourselves ;)")

    # return the cleaned data
    return data




class EmployeeDirectReportAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.employee = kwargs.pop('employee', None)
    super(EmployeeDirectReportAddForm, self).__init__(*args, **kwargs)

  direct_report_email = forms.EmailField(
    required = True,
    max_length = 70,
    label = mark_safe("""<i>To add an employee to this manager's team, enter the employee's email address</i>"""),
    validators=[valid_email_lower, valid_employee_email_exists],
  )

  adopt_new_departments = AdoptNewDepartments()
  drop_old_departments = DropOldDepartments()
  drop_old_bosses = DropOldBosses()

  def clean_direct_report_email(self):
    boss = self.employee
    data = self.cleaned_data['direct_report_email']
    direct_report = Employee.nodes.get(email_work=data)

    if direct_report.bosses.is_connected(boss) == True:
      raise ValidationError("Sorry, that employee already reports to that manager. Cannot create duplicate relationship.")

    if boss == direct_report:
      raise ValidationError("Let's be real, we can't manage ourselves ;)")

    # return the cleaned data
    return data




#------------------------ DEPARTMENTS ------------------------ 

class DepartmentForm(forms.ModelForm):
  class Meta:
    model = Department
    fields = [
      'name_long', 'name_short', 
      'mission_statement',
      'email_distribution', 'channel_slack',
    ]


  name_long = forms.CharField(
    required = True,
    max_length = 65,
    label = mark_safe("""<b>DEPARTMENT NAME</b><i><br/>Does not have to be unique e.g. "Pre-sales Engineering Team")</i>"""),
  )

  name_short = forms.CharField(
    required = True, # validators are not run against empty fields
    max_length = 51,
    label = mark_safe("""<br/><b>DEPARTMENT ALIAS</b><br/><i>Used to identify the team with a leading @ symbol "@sales_engineering_hydraulics_usa"</i>"""),
    validators=[valid_dept_alias_chars,],
  )

  mission_statement = forms.CharField(
    required = False,
    max_length = 500,
    label = mark_safe("""<br/><b>MISSION STATEMENT</b><br/><i>Optional -- Why the team comes to work in the morning & what you all do in 500 chars or less</i>"""),
    widget=forms.Textarea(attrs={'class': "mission-text-area", 'cols': 80, 'rows': 3}),
  )

  email_distribution = forms.EmailField(
    required = False,
    max_length = 254,
    label = mark_safe("""<i>Email Distribution List</i>"""),
    validators=[valid_email, valid_email_lower],
  )


  error_messages = {
    'name_long': {
      'max_length': _("The max length is 65 characters."),
    },
    'name_short': {
      'max_length': _("The max length is 51 characters, including the @ symbol."),
    },
    'mission_statement': {
      'max_length': _("500 chars max. Dale Carnegie - 'I would have written a shorter letter, but I did not have the time.'"),
    },
  }




class DepartmentEmployeeAdd(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.department = kwargs.pop('department', None)
    super(DepartmentEmployeeAdd, self).__init__(*args, **kwargs)

  member_email = forms.EmailField(
    required = True,
    max_length = 70,
    label = mark_safe("""<i>To add an employee to this department, enter the employee's email address</i>"""),
    validators=[valid_email_lower, valid_employee_email_exists],
  )

  is_manager = DepartmentManager() # note, this does not appear in the original model. it triggers the connect on department_manager rel
  drop_old_departments = DropOldDepartments()

  def clean_member_email(self):
    department = self.department
    data = self.cleaned_data['member_email']
    member = Employee.nodes.get(email_work=data)

    if member.departments.is_connected(department) == True:
      raise ValidationError("Sorry, that employee already belongs to this team. Cannot create duplicate relationship.")

    # return the cleaned data
    return data




class EmployeeDepartmentAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.employee = kwargs.pop('employee', None)
    super(EmployeeDepartmentAddForm, self).__init__(*args, **kwargs)

  dept_alias = forms.CharField(
    required = True,
    max_length = 51,
    label = mark_safe("""Join Department (provide its alias e.g. @sales_leadership)"""),
    validators=[valid_dept_alias_chars, valid_dept_alias_exists,],
  )

  is_manager = DepartmentManager() # note, this does not appear in the original model. it triggers the connect on department_manager rel
  drop_old_departments = DropOldDepartments()

  def clean_dept_alias(self):
    employee = self.employee
    data = self.cleaned_data['dept_alias']
    department = Department.nodes.get(name_short=data)

    if employee.departments.is_connected(department) == True:
      raise ValidationError("Sorry, that employee already belongs to this team. Cannot create duplicate relationship.")

    # return the cleaned data
    return data




class DepartmentParentAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.department = kwargs.pop('department', None)
    super(DepartmentParentAddForm, self).__init__(*args, **kwargs)

  parent_department_alias = forms.CharField(
    required = True,
    max_length = 51,
    label = mark_safe("""New Parent Department (provide their alias e.g. @sales_leadership)"""),
    validators=[valid_dept_alias_chars, valid_dept_alias_exists,],
  )

  drop_old_parents = DropOldParents()

  def clean_parent_department_alias(self):
    child_department = self.department
    data = self.cleaned_data['parent_department_alias']
    parent_department = Department.nodes.get(name_short=data)

    if child_department.parent_departments.is_connected(parent_department) == True:
      raise ValidationError("Sorry, this department is already a branch of the parent department that you entered. Cannot create duplicate relationship.")

    if child_department == parent_department:
      raise ValidationError("No departmentception allowed ;)")

    # return the cleaned data
    return data




class DepartmentBranchAdd(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.department = kwargs.pop('department', None)
    super(DepartmentBranchAdd, self).__init__(*args, **kwargs)

  branch_department_alias = forms.CharField(
    required = True,
    max_length = 51,
    label = mark_safe("""New Branch Department (provide their alias e.g. @business_development)"""),
    validators=[valid_dept_alias_chars, valid_dept_alias_exists,],
  )

  drop_old_parents = DropOldParents()

  def clean_branch_department_alias(self):
    parent_department = self.department
    data = self.cleaned_data['branch_department_alias']
    child_department = Department.nodes.get(name_short=data)

    if parent_department.branches.is_connected(child_department) == True:
      raise ValidationError("Sorry, this department is already a parent of the branch department that you entered. Cannot create duplicate relationship.")

    if child_department == parent_department:
      raise ValidationError("No departmentception allowed ;)")

    # return the cleaned data
    return data




class DeptMergeDeleteForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.department = kwargs.pop('department', None)
    super(DeptMergeDeleteForm, self).__init__(*args, **kwargs)
  
  into_alias = forms.CharField(
    required = False,
    max_length = 51,
    label = mark_safe("""When you delete this department, do you want to merge its members into another department? If so, please provide the alias of the department that you want to merge these employees into "e.g. @tech_support")"""),
    validators=[valid_dept_alias_chars, valid_dept_alias_exists,],
  )

  def clean_into_alias(self):
    dept_drop = self.department
    data = self.cleaned_data['into_alias']

    try:
      dept_into = Department.nodes.get(name_short=data)
    except:
      dept_into = None
      
    if dept_into is not None:
      if dept_into == dept_drop:
        raise ValidationError("You cannot merge your members into the same department that you are about to drop ;)")

    # return the cleaned data
    return data




#------------------------ OFFICES ------------------------ 

class OfficeForm(forms.ModelForm):
  class Meta:
    model = Office
    fields = [
      'name_long', 'name_short', 
      'address_name', 'address_street', 'address_street_ext', 'address_city',
      'address_state', 'address_zip', 'address_country', 
      'phone_main_desk', 'email_distribution', 'channel_slack',
    ]

  name_long = forms.CharField(
    required = True,
    max_length = 65,
    label = mark_safe("""<b>OFFICE NAME</b><i><br/>Does not have to be unique e.g. "Boston - MIT")</i>"""),
  )

  name_short = forms.CharField(
    required = True, # validators are not run against empty fields
    max_length = 51,
    label = mark_safe("""<br/><b>OFFICE ALIAS</b><br/><i>Used to identify the office with a leading & symbol "&boston-mit"</i>"""),
    validators=[valid_office_alias_chars,],
  )

  email_distribution = forms.EmailField(
    required = False,
    max_length = 254,
    label = mark_safe("""<i>Email Distribution List</i>"""),
    validators=[valid_email, valid_email_lower],
  )

  error_messages = {
    'name_long': {
      'max_length': _("The max length is 65 characters."),
    },
    'name_short': {
      'max_length': _("The max length is 51 characters, including the & symbol."),
    },
  }




class OfficeEmployeeAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.department = kwargs.pop('office', None)
    super(OfficeEmployeeAddForm, self).__init__(*args, **kwargs)

  employee_email = forms.EmailField(
    required = True,
    max_length = 70,
    label = mark_safe("""<i>To add an employee to this office, enter the employee's email address</i>"""),
    validators=[valid_email_lower, valid_employee_email_exists],
  )

  drop_old_offices = EmployeeDropOldOffices()
  is_manager = OfficeManager() # note, this does not appear in the original model. it triggers the connect on officer_manager rel

  def clean_member_email(self):
    office = self.office
    data = self.cleaned_data['employee_email']
    employee = Employee.nodes.get(email_work=data)

    if employee_email.offices.is_connected(office) == True:
      raise ValidationError("Sorry, that employee already works at that office. Cannot create duplicate relationship.")

    # return the cleaned data
    return data




class EmployeeOfficeAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.employee = kwargs.pop('employee', None)
    super(EmployeeOfficeAddForm, self).__init__(*args, **kwargs)

  office_alias = forms.CharField(
    required = True,
    max_length = 51,
    label = mark_safe("""Join this Office (provide its alias e.g. &boston-mit)"""),
    validators=[valid_office_alias_chars, valid_office_alias_exists,],
  )

  is_manager = OfficeManager() # note, this does not appear in the original model. it triggers the connect on office_manager rel
  drop_old_offices = EmployeeDropOldOffices

  def clean_office_alias(self):
    employee = self.employee
    data = self.cleaned_data['office_alias']
    office = Office.nodes.get(name_short=data)

    if employee.offices.is_connected(office) == True:
      raise ValidationError("Sorry, that employee works at that office. Cannot create duplicate relationship.")

    # return the cleaned data
    return data




class OfficeMergeDeleteForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.office = kwargs.pop('office', None)
    super(OfficeMergeDeleteForm, self).__init__(*args, **kwargs)
  
  into_alias = forms.CharField(
    required = False,
    max_length = 51,
    label = mark_safe("""When you delete this office, do you want to merge its departments and employees into another office? If so, please provide the alias of the office that you want to merge them into "e.g. &boston-seaport")"""),
    validators=[valid_office_alias_chars, valid_office_alias_exists,],
  )

  def clean_into_alias(self):
    office_drop = self.office
    data = self.cleaned_data['into_alias']

    try:
      office_into = Department.nodes.get(name_short=data)
    except:
      office_into = None
      
    if office_into is not None:
      if office_into == office_drop:
        raise ValidationError("You cannot merge your employees into the same office that you are about to drop ;)")

    # return the cleaned data
    return data




class OfficeDepartmentAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.office = kwargs.pop('office', None)
    super(OfficeDepartmentAddForm, self).__init__(*args, **kwargs)

  department_alias = forms.CharField(
    required = True,
    max_length = 51,
    label = mark_safe("""Department to be based in office (provide their alias e.g. @sales_leadership)"""),
    validators=[valid_dept_alias_chars, valid_dept_alias_exists,],
  )

  drop_old_offices = DepartmentDropOldOffices()
  merge_employees = OfficeMergeEmployees()

  def clean_department_alias(self):
    office = self.office
    data = self.cleaned_data['department_alias']
    department = Department.nodes.get(name_short=data)

    if department.offices.is_connected(office) == True:
      raise ValidationError("Sorry, that department is already based in that office. Cannot create duplicate relationship.")

    # return the cleaned data
    return data




class DepartmentOfficeAddForm(forms.Form):
  def __init__(self, *args, **kwargs): 
    self.department = kwargs.pop('department', None)
    super(DepartmentOfficeAddForm, self).__init__(*args, **kwargs)

  office_alias = forms.CharField(
    required = True,
    max_length = 51,
    label = mark_safe("""Office the department should be based in (provide its alias e.g. &boston-kendall)"""),
    validators=[valid_office_alias_chars, valid_office_alias_exists,],
  )

  drop_old_offices = DepartmentDropOldOffices()
  merge_employees = OfficeMergeEmployees()

  def clean_office_alias(self):
    department = self.department
    data = self.cleaned_data['office_alias']
    office = Office.nodes.get(name_short=data)

    if department.offices.is_connected(office) == True:
      raise ValidationError("Sorry, that department is already based in that office. Cannot create duplicate relationship.")

    # return the cleaned data
    return data
