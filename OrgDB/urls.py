from django.contrib import admin
from django.urls import include, path
from django.contrib.auth import views as auth_views
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

# This is the project-level urls file. The include() function allows referencing other URLconfs. Whenever Django encounters include(), it chops off whatever part of the URL matched up to that point and sends the remaining string to the included URLconf for further processing.

urlpatterns = [
    path('admin/', admin.site.urls),
    # s3upload package has its own urls.py file for handling get_upload_params
    # Don't need to put upload forms in a folder called s3upload/
    path('s3upload/', include('s3upload.urls')),
    path('', include('orgchart.urls')),
]

# this will only work if debug mode is set to TRUE
urlpatterns += staticfiles_urlpatterns()
