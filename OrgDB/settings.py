"""
Django settings for `OrgDB` project.
Generated by 'django-admin startproject' using Django 2.0.1.

For more information on this file, see
https://docs.djangoproject.com/en/2.0/topics/settings/
For the full list of settings and their values, see
https://docs.djangoproject.com/en/2.0/ref/settings/
"""

import os

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
# we use BASE_DIR instead of PROJECT_DIR
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
FIXTURE_DIRS = (
   os.path.join(BASE_DIR, 'fixtures'),
)


# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/2.0/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'v9##%c&vz(8i2evkl%k8s_ijv&=xhc7fb58gy*n#as&#n@8-!!'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True


ALLOWED_HOSTS = []


# https://github.com/neo4j-contrib/django-neomodel
# 2nd param is the ...//neo4j:password where neo4j is the user and you set a pw when creating the db aka graph
NEOMODEL_NEO4J_BOLT_URL = os.environ.get('NEO4J_BOLT_URL', 'bolt://neo4j:ijkjijkji@localhost:7687')

# Application definition
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django_extensions',
    'django_neomodel',
    's3upload',
    'orgchart',
]


MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'OrgDB.urls'

# gets rid of the trailing slash so that you can have clean urls like /employees
# when set to True, if the request URL does not match any of the patterns in the URLconf and it doesn’t end in a slash, an HTTP redirect is issued to the same URL with a slash appended.
APPEND_SLASH = True

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates'),],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
            'debug': DEBUG, # development
        },
    },
]

# bootstrap messages that are closeable https://simpleisbetterthancomplex.com/tips/2016/09/06/django-tip-14-messages-framework.html
from django.contrib.messages import constants as messages
MESSAGE_TAGS = {
    messages.DEBUG: 'alert-info',
    messages.INFO: 'alert-info',
    messages.SUCCESS: 'alert-success',
    messages.WARNING: 'alert-warning',
    messages.ERROR: 'alert-danger',
}

WSGI_APPLICATION = 'OrgDB.wsgi.application'


DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'orgdb',
        'USER': 'laynetrain',
        'PASSWORD': '',
        'HOST': 'localhost',
        'PORT': '5432',
    }
}


# Password validation
# https://docs.djangoproject.com/en/2.0/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]

# Internationalization
# https://docs.djangoproject.com/en/2.0/topics/i18n/

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/2.0/howto/static-files/

STATIC_URL = '/static/'
STATICFILES_DIRS = (
    os.path.join(BASE_DIR, 'static'), #don't delete trailing comma

)


# https://simpleisbetterthancomplex.com/tutorial/2016/06/27/how-to-use-djangos-built-in-login-system.html
LOGIN_REDIRECT_URL = 'employee_list'

# https://www.fomfus.com/articles/how-to-use-email-as-username-for-django-authentication-removing-the-username
AUTH_USER_MODEL = 'orgchart.User'


# During development only
# https://simpleisbetterthancomplex.com/tutorial/2016/09/19/how-to-create-password-reset-view.html
EMAIL_BACKEND = 'django.core.mail.backends.console.EmailBackend'


# ======================== S3 ========================
# s3upload package -- https://github.com/yunojuno/django-s3-upload
# arn policies -- https://medium.com/@manibatra23/setting-up-amazon-s3-bucket-for-serving-django-static-and-media-files-3e781ab325d5

# would there be a benefit of using `MEDIA_URL =''` setting?

# these are the credentials of the IAM user 'jamazon'
# that belongs to the IAM group 'orgdb-assets-s3'
AWS_ACCESS_KEY_ID = 'AKIA5DSW5DT34LMTJ4NW'
AWS_SECRET_ACCESS_KEY = '4iocLFug4AyQ1lWPUd7VGT8rZ4u1s2Wo59aYBiWs'
# "remove query parameter authentication from generated URLs. This can be useful if your S3 buckets are public."
AWS_QUERYSTRING_AUTH = False 

AWS_STORAGE_BUCKET_NAME = 'orgdb-assets'
S3UPLOAD_REGION = 'us-east-1'


def create_filename_media_folder(filename):
    import uuid
    extension = filename.split('.')[-1] # get everything after dot
    filename = '%s.%s' % (uuid.uuid4().hex, extension) # uid before, ext after
    return os.path.join('media', filename)


S3UPLOAD_DESTINATIONS = {
    'media_folder': {
        'key': create_filename_media_folder,
        # optional params:
        # i've had some troubles with svg width/ margins
        'allowed_types': ['image/jpeg', 'image/jpg', 'image/png'],
        'allowed_extensions': ('.jpeg', '.jpg', '.png',),
        # file size 1KB-25MB. Error = `Sorry, the file is too large to be uploaded.`
        # does not give option to refresh or anything when erroring
        'content_length_range': (1000, 25000000), 
    }
}
